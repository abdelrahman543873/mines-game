import { Component } from '@angular/core';

interface Cell {
  type: 'hidden' | 'bomb' | 'diamond';
  revealed: boolean;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'mines-game';
  gridSize = 5;
  totalCells = this.gridSize * this.gridSize;
  cells!: Cell[][];
  bombs = 5;
  betAmount: number = 0;
  accumulatedWinnings = 0;
  gameOver = false;
  multiplier: number = 0;
  initialBet = 0;

  ngOnInit() {
    this.initGame();
    this.placeBombs();
    this.revealDiamonds();
  }

  initGame() {
    this.cells = Array.from({ length: this.gridSize }, () =>
      Array(this.gridSize).fill({ type: 'hidden', revealed: false })
    );
  }

  startNewGame(betAmount: number) {
    this.betAmount = betAmount;
    this.initGame();
    this.placeBombs();
    this.revealDiamonds();
  }

  placeBombs() {
    let bombsPlaced = 0;
    while (bombsPlaced < this.bombs) {
      const row = Math.floor(Math.random() * this.gridSize);
      const col = Math.floor(Math.random() * this.gridSize);
      if (this.cells[col][row].type !== 'bomb') {
        this.cells[col][row] = { type: 'bomb', revealed: false };
        bombsPlaced++;
      }
    }
  }

  revealDiamonds() {
    for (let i = 0; i < this.gridSize; i++) {
      for (let j = 0; j < this.gridSize; j++) {
        if (this.cells[i][j].type !== 'bomb') {
          this.cells[i][j] = { type: 'diamond', revealed: false };
        }
      }
    }
  }

  cellClicked(row: number, col: number) {
    if (this.gameOver || this.cells[row][col].revealed) {
      return;
    }

    this.cells[row][col].revealed = true;

    if (this.cells[row][col].type === 'bomb') {
      this.handleBombClick();
    } else {
      this.handleDiamondClick();
    }
  }

  handleBombClick() {
    this.gameOver = true;
    this.betAmount = 0;
  }

  handleDiamondClick() {
    const numberOfDiamonds = this.countRevealedDiamonds();
    const payoutMultiplier =
      1 / (this.bombs / (this.totalCells - numberOfDiamonds));
    const amountWon = this.betAmount * payoutMultiplier;
    if (this.accumulatedWinnings === 0 && amountWon > 0) {
      this.initialBet = this.betAmount;
    }
    this.betAmount = amountWon;
    this.accumulatedWinnings = +(this.accumulatedWinnings + amountWon).toFixed(
      2
    );
  }

  countRevealedDiamonds(): number {
    let count = 0;
    for (const row of this.cells) {
      for (const cell of row) {
        if (cell.revealed && cell.type === 'diamond') {
          count++;
        }
      }
    }
    return count;
  }

  cashOut() {
    this.gameOver = true;
    this.multiplier = +(this.accumulatedWinnings / this.initialBet).toFixed(2);
  }
}

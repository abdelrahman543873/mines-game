import { Component, EventEmitter, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-betting-board',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './betting-board.component.html',
  styleUrl: './betting-board.component.css',
})
export class BettingBoardComponent {
  betAmount: number = 0;
  isGameStarted: boolean = false;
  @Output() newGame = new EventEmitter<number>();
  @Output() cashOut = new EventEmitter();

  controlGame() {
    this.isGameStarted = !this.isGameStarted;
    if (this.isGameStarted) this.newGame.emit(this.betAmount);
    if (!this.isGameStarted) this.cashOut.emit();
  }
}

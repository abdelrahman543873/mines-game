import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BettingBoardComponent } from './betting-board/betting-board.component';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, FormsModule, BettingBoardComponent],
  bootstrap: [AppComponent],
})
export class AppModule {}
